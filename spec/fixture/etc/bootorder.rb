virtual_machine "bootorder" do
  # Using a local device as disk:
  disk '/dev/null'

  # Boot from disk, then from cdrom
  boot order: 'cd', once: 'dc', menu: 'on'
end