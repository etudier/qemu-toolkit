require 'spec_helper'

require 'qemu-toolkit/network/mac_address'

describe QemuToolkit::Network::MacAddress do
  let(:addr) { described_class.new('0:24:81:63:58:51') }

  it "should convert to a normalized string" do
    addr.to_s.should == '00:24:81:63:58:51'
  end
  it "should correctly handle a degenerate case" do
    described_class.new('0:1:2:3:4:5').to_s.should == '00:01:02:03:04:05'
  end
  
  describe 'malformed addresses' do
    [
      '0:1:2:3:4:5:6',    # too many elements
      '0:1:2',            # not enough elements
      '123:1:2:3:4:5'
    ].each do |malformed|
      it "should fail for #{malformed}" do
        expect {
          described_class.new(malformed)
        }.to raise_error
      end
    end
  end

  describe 'comparison' do
    def a str
      described_class.new(str)
    end

    it "has a working equality operator" do
      a('0:1:2:3:4:5').should == a('0:1:2:3:4:5')
      a('0:1:2:3:4:5').should_not == a('0:1:2:3:4:6')

      a('a:b:c:d:e:f').should == a('A:B:C:D:E:F')

      a('a:b:c:d:e:f').should == a('0a:0b:0c:0d:0e:0f')
    end  
  end
end