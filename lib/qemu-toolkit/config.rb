module QemuToolkit

  # A configuration class that acts as a singleton, but really isn't. This 
  # allows for a mixed style of coding, either using QemuToolkit::Config as 
  # an instance or as a class. 
  #
  class Config
    class << self # CLASS METHODS
      def method_missing(sym, *args, &block)
        return current.send(sym, *args, &block) if current.respond_to?(sym)
        super
      end
      def respond_to?(sym)
        current.respond_to?(sym) || super
      end
      
      def current
        @current ||= new
      end
      def reset 
        @current = nil
      end
    end

    attr_writer :etc
    attr_writer :var_run
    
    def etc(*args)
      expand_path(@etc, *args)
    end
    def var_run(*args)
      expand_path(@var_run, *args)
    end
    
    # The command runner backend for all commands. Actual execution happens
    # through this instance. This has advantages for testing and for 
    # customizing behaviour. 
    #
    def backend
      @backend ||= Backend::Illumos.new
    end
  private
    def expand_path(*args)
      File.expand_path(
        File.join(*args))
    end
  end
end