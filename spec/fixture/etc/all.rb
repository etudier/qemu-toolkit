virtual_machine "all" do
  # Boot from disk, then from cdrom
  boot order: 'cd'

  # Using a local device as disk:
  disk '/dev/null'
  
  # Importing all LUNs of an iSCSI target as disks:
  iscsi_target 'iqn.2010-01.com.qemu-toolkit:vm1', "10.0.30.1"
  
  # Manual drive configuration:
  drive if: 'floppy', file: 'floppy.dsk'

  # Controlling how many cores are used by this machine:
  cpus 4
  
  # RAM:
  ram  2048   # these are mbs 
  
  # Automatic vnic network configuration:
  nic 'eth0', 
    macaddr: '2:8:20:52:a6:7e', 
    model: 'virtio', 
    via: 'igb1'
    
  # Manual network configuration:
  net :vnic, vlan: 1, name: 'vm1', ifname: 'vm1', macaddr: '1:8:20:52:a6:7e'
  net :nic, vlan: 1, name: "vm1", model: "virtio", macaddr: '1:8:20:52:a6:7e'
  
  # Make vnc listen on a given port: (5900 + display = port)
  vnc_display ':0'
  
  # Change the keyboard layout (-k option on qemu)
  keyboard_layout 'de-ch'
  
  # Arguments that are appended to the qemu launch command line: 
  extra_arg '-foo'
end
