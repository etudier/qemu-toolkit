require 'spec_helper'

require 'qemu-toolkit/vnic'

require 'support/backend_fake'

describe QemuToolkit::Vnic do
  let(:backend) { BackendFake.new(
    /^dladm show-vnic/ => %Q(vm0_1:igb1:0:0\\:2\\:8\\:20\\:b5\\:8c
vm1_2:igb1:0:0\\:2\\:8\\:20\\:b5\\:8f
vm1_1:igb1:0:0\\:2\\:8\\:20\\:b5\\:8e
vm1_3:igb2:0:0\\:2\\:8\\:20\\:b5\\:90
vm2_1:igb3:1:0\\:2\\:8\\:20\\:b5\\:91
vm2_2:igb3:2:0\\:2\\:8\\:20\\:b5\\:92)) }

  def vnic(*args)
    described_class.new(*args)
  end
  
  describe '.for_prefix(prefix)' do
    it "returns a map of physical to virtual nics" do
      collection = QemuToolkit::Vnic.for_prefix('vm0', backend)

      collection.allocate('igb1').
        should == vnic('vm0', 1, 'igb1', '0:2:8:20:b5:8c')

      collection = QemuToolkit::Vnic.for_prefix('vm1', backend)

      collection.allocate('igb1').
        should == vnic('vm1', 2, 'igb1', '0:2:8:20:b5:8f')
      collection.allocate('igb1').
        should == vnic('vm1', 1, 'igb1', '0:2:8:20:b5:8e')

      collection.allocate('igb2').
        should == vnic('vm1', 3, 'igb2', '0:2:8:20:b5:90')

      collection.should be_empty
    end 
    it "returns a map of vnic to virtual nics" do
      collection = QemuToolkit::Vnic.for_prefix('vm2', backend)

      collection.allocate('igb3:1').should == 
        vnic('vm2', 1, 'igb3:1', '0:2:8:20:b5:91')

      collection.allocate('igb3:2', '0:2:8:20:b5:92').should == 
        vnic('vm2', 2, 'igb3:2', '0:2:8:20:b5:92')

      collection.should be_empty
    end 
  end
  describe '.create(vm_name, iface)' do
    it "creates the link" do
      QemuToolkit::Vnic.create('vm3', 'igb1', backend).should == vnic('vm3', 1, 'igb1')

      backend.commands.last.should == "dladm create-vnic -l igb1 vm3_1"
    end
    it "creates the link with the correct VLAN id" do
      QemuToolkit::Vnic.create('vm3', 'igb1:3', backend).should == vnic('vm3', 1, 'igb1:3')

      backend.commands.last.should == "dladm create-vnic -l igb1 -v 3 vm3_1"
    end 
    it "creates the link with VLAN and --mac-address" do
      QemuToolkit::Vnic.create('vm3', 'igb1:3', backend, '0:2:8:20:b5:4').should == 
        vnic('vm3', 1, 'igb1:3', '0:2:8:20:b5:4')

      backend.commands.last.should == "dladm create-vnic -l igb1 -v 3 --mac-address 00:02:08:20:b5:04 vm3_1"
    end
  end
end