module QemuToolkit
  class Backend::Illumos
    attr_accessor :verbose
    
    def zfs *args
      run_cmd :zfs, *args
    end
    def itadm(*args)
      run_cmd :itadm, *args
    end
    def stmfadm *args
      run_cmd :stmfadm, *args
    end
    def qemu name, args
      exec_with_arg0 '/usr/bin/qemu-system-x86_64', name, *args
    end
    def iscsiadm *args
      run_cmd :iscsiadm, *args
    end
    def dladm *args
      run_cmd :dladm, *args
    end
    
    def disks(path)
      output = zfs :list, '-H -o name -t volume', '-r', path
      output.split("\n")
    end
    
    def run_cmd(*args)
      cmd = args.join(' ')
      puts cmd if verbose
      ret = %x(#{cmd} 2>&1)

      raise "Execution error: #{cmd}." unless $?.success?

      ret
    end
    def exec_with_arg0(command, name, *args)
      # exec in this form only wants to be given raw arguments that contain
      # no spaces. This is why we split and flatten args. 
      exec [command, name], *args.map { |a| a.split }.flatten
      
      fail "exec failed."
    end
  end
end